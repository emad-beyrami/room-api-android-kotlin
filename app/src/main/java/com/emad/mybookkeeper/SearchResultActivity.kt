package com.emad.mybookkeeper

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.google.android.material.floatingactionbutton.FloatingActionButton
import androidx.appcompat.app.AppCompatActivity
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.room.ps.bookkeeper.BookListAdapter
import kotlinx.android.synthetic.main.content_main.*
import java.util.*

class SearchResultActivity : AppCompatActivity(), BookListAdapter.OnDeleteClickListener {

    private lateinit var searchViewModel: BookViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        val bookListAdapter = BookListAdapter(this, this)
        recyclerView.adapter = bookListAdapter

        recyclerView.layoutManager = LinearLayoutManager(this)

        findViewById<FloatingActionButton>(R.id.fab).visibility = View.INVISIBLE

        searchViewModel = ViewModelProviders.of(this).get(BookViewModel::class.java)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == SearchResultActivity.UPDATE_BOOK_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val id = UUID.randomUUID().toString()
            val authorName = data?.getStringExtra(NewBookActivity.NEW_AUTHOR)
            val bookName = data?.getStringExtra(NewBookActivity.NEW_BOOK)

            val book = Book(id, authorName, bookName)
            searchViewModel.insert(book)

            Toast.makeText(
                applicationContext, R.string.saved, Toast.LENGTH_LONG
            ).show()


        } else
            Toast.makeText(applicationContext, R.string.not_saved, Toast.LENGTH_LONG).show()
    }

    override fun onDeleteClickListener(myBook: Book) {
        searchViewModel.delete(myBook)
        Toast.makeText(applicationContext, R.string.deleted, Toast.LENGTH_LONG).show()
    }

    companion object {
        const val UPDATE_BOOK_ACTIVITY_REQUEST_CODE = 2
    }

}