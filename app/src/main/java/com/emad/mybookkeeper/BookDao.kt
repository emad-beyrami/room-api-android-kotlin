package com.emad.mybookkeeper

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface BookDao {

    @Insert
    fun insert(book: Book)

//    @Query("select * from  books")
//    fun getAllBooks(): LiveData<List<Book>>

    @get:Query("select * from books")
    val allBooks: LiveData<List<Book>>

    @Update
    fun update(book: Book)

    @Delete
    fun delete(book: Book)
}